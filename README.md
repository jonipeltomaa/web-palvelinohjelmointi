# Web-palvelinohjelmointi
## Harjoitukset 1
Harjoituksessa 1 Piti selvittää noden avulla käyttöjärjestelmän nykyisen käyttäjä käyttämällä conosle.log() metodia. <br>
Sekä selvittämään järjestelmän käynnissä oloaika (Uptime) ja kokonaismuistin määrä käyttämällä node.js sisäänrakennettua os-moduulia.
## Harjoituksen 2 
### Harjoituksen 2 käytetyt npm moduulit.
- npm init
- npm i -D nodemon
- npm i express
### Harjoituksen 2 kuvaus
Harjoituksessa 2 ensiksi piti toteutaa albumikokoelma sovellus käyttäen Node Express-sovelluskehystä jolla ohjataan eri pyynnöt (GET, POST, PUT, DELETE).<br>
Toisena piti nykyistä toteutusta muuttaa siten että luotiin kansio hierarkia Express reiteille.<br>
Kolmanneksi piti tehdä yksinkertainen logger-middleware jolla voidaan etsiä tietystä reitistä kysaelymerkkijono (query string). <br> 
Jos kyseistä merkki jono ei löydy tietokannasta annetussa pyynön yhteydessä, lähetetään takaisin "Unauthorized" vastaus.<br>
Neljänneksi luotiin index.html-tiedosto joka sijoitettiin public kansion alle. Sekä määriteltiin Express näyttämään public-hakemiston sisältö "express.static()" -funktiolla.<br>
Sivun latauksen yhteydessä osoitteesta (localhost:portin numero) sivulla listattiin nykyisen albumikokoelman albumit, tiedot saatiin API-kyselyn kautta.
### Harjoituksen 2 kuvia
GET-Pyyntö jolla näytetään pelkä linkki joka ohjaa ohjelmassa käytettävään rajapintaan.<br>
![Harjoitus2](Kuvia/GETpyyntö1.png "harjoitus2")<br>
GET-pyyntö jolla haetaan albumi tietokannasta kaikki albumit.<br>
![Harjoitus2](Kuvia/GETpyyntö2.png "harjoitukset2")<br>
GET-pyyntö jolla haetaan pelkästään yhtä levyn tietoa tietokannasta.<br>
![Harjoitus2](Kuvia/GETpyyntö3.png "harjoitus2")<br>
POST-Pyynnöllä voidaan luoda uusi albumi tietokantaan.<br>
![Harjoitukset2](Kuvia/POSTpyyntö3.png "harjoitus2") <br>
PUT-pyynöllä voidaan muokata albumi tietokannasta, albumin tietoja. <br>
![Harjoitukset2](Kuvia/PUTpyyntö3.png "harjoitukset2") <br>
![Harjoitukset2](Kuvia/GETtarkistuspyyntö3.png "harjoitukset2") <br>
Verkkosivu näkymä <br>
![Harjoitukset2](Kuvia/Verkkosivu%20näkymä.png "harjoitukset2") <br>
## Harjoitukset 3
### Harjoitukset 3 käytetyt npm moduulit
- npm i mongoose
- npm i dotenv
### Harjoitukset 3 kuvaus
Harjoitukset 2 tietokanta oli data.js tiedostona, haarjoitukset 3 otettiin käytöön mongodb tietokanta ja mongoose moduuli.<br>
Harjoituksessa piti myös muokata Express-reittejä siten että CRUD pyynnöt tallentuvat tietokantaan.<br>
Tietokanta on alussa tyhjä.<br>
Dotenv moduulilla voidaan piilottaa tietokannan käytetyt henkilö kohtaiset osoitteet ja api avaimet.
### Harjoitukset 3 kuvat
POST-pyyntö jolla luodaan uusi albumi, albumi tietokantaan mongodb:ssä.<br>
![Harjoitukset3](Kuvia/harjoitukset3/postpyynto.png "harjoitukset3")<br>
GET-pyyntö joka näyttää kaikki tallettetut albumit mongodb tietokannasta.<br>
![Harjoitukset3](Kuvia/harjoitukset3/getpyynto.png "harjoitukset3")<br>
GET-pyyntö jolla näytetään valitun id perusteella olevan albumin tieto mongodb tietokannasta.<br>
![Harjoitukset3](Kuvia/harjoitukset3/getidpyynto.png "harjoitukset3")<br>
PUT-pyynöllä voidaan muokata albumi tietoja mongodb tietokannasta.<br>
![Harjoitukset3](Kuvia/harjoitukset3/putpyynto.png "harjoitukset3")<br>
DELETE-pyyntö poistetaan id perusteella yksi albumin tieto mongodb tietokannasta.<br>
![Harjoitukset3](Kuvia/harjoitukset3/deletepyynto.png "harjoitukset3")<br>
Viimeiseksi kuva mongoDB tietokannasta.<br>
![Harjoitukset3](Kuvia/harjoitukset3/mongodb.png "harjoitukset3")