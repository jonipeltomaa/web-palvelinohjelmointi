const os = require('os');

// Hakee järjestelmän käynissäoloajan (uptime) sekunneissa
const uptimeInSeconds = os.uptime();

// Muuntaa käyttöajan päiviksi
const uptimeInMinutes = uptimeInSeconds / 60;
const uptimeInHours = uptimeInMinutes / 60;
const uptimeInDays = uptimeInHours / 24;

console.log(`System uptime: ${uptimeInDays.toFixed(2)} days`);

// Hakee järjestelmä muisti tiedon bitteinä
const totalMemoryInBytes = os.totalmem();

// Muuttaa järjestelmä muistin Gigabiteiksi
const totalMemoryInKB = totalMemoryInBytes / 1024;
const totalMemoryInMB = totalMemoryInKB / 1024;
const totalMemoryInGB = totalMemoryInMB / 1024;

console.log(`Total memory: ${totalMemoryInGB.toFixed(2)} GB`);