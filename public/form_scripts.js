const result = document.querySelector('.result')
const baseUrl = `${window.location.origin}/api`
const fetchAlbums = async () => {
  try {
    const { data } = await axios.get(`${baseUrl}/albums`)

    const albums = data.map((album) => {
      return `<ul><li>Levy: ${album.title}</li><li>Artisti: ${album.artist}</li><li>Ilmestymis vuosi: ${album.year}</li></ul>`
    })
    result.innerHTML = albums.join('')
  } catch (error) {
    console.log(error)
    result.innerHTML = `<div class="alert alert-danger">Could not fetch data</div>`
  }
}
fetchAlbums()
