// Muuttujien alustus
require('dotenv').config()
const express = require('express')
const connectMongoDB = require('./db/mongodb')
const { CONN_STRING } = process.env
const app = express()


app.use(express.json())

const albums = require('./routes/albums')
const logger = require('./middleware/logger')

app.use('/api/albums', albums)

// static assets
app.use(express.static('./public'))



// Etusivulta näytetään pelkä linkki joka ohjaa ohjelmassa käytettävään rajapintaan.
app.get('/', (req, res) => {
    res.send('<h1>API front</h1><a href="/api/albums">Albumit</a>')
})

connectMongoDB(CONN_STRING)

const PORT = 5001
app.listen(PORT, () => {
    console.log(`Serveri kuuntelee porttia ${PORT}...`)
})