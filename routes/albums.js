const express = require('express')
const router = express.Router()

const {
    getAlbums,
    getSingleAlbum,
    deleteAlbum,
    updateAlbum,
    createAlbum,
} = require('../controllers/albums')

router.get('/', getAlbums)
router.get('/:albumID', getSingleAlbum)
router.delete('/:albumID', deleteAlbum)
router.put('/:albumID', updateAlbum)
router.post('/', createAlbum)
module.exports = router