const Album = require('../models/Album')

// Get pyyntö jolla haetaan kaikki albumit tietokannasta
const getAlbums = async (req,res) => {
    const album = await Album.find({})
    res.status(200).json(album)
}

// GET pyyntö jolla haetaan yksi albumi tietokannasta
const getSingleAlbum = async (req, res) => {
    const { albumID } = req.params
    try{
        const sigleAlbum = await Album.findById(albumID)
        // Tarkistus onko käyttäjä JSON-datassa vai lähetetäänkö 404
        if (!sigleAlbum) {
            return res.status(404).json('Albumia ei löytynyt')
        }
        return res.status(200).json(sigleAlbum)
    }
    catch (error) {

    }
}

// DELETE pyyntö poistaa yhden albumi tietokannasta
const deleteAlbum = async (req, res) => {
    const { albumID } = req.params
    try {
        const album = await Album.findById(albumID)
        if (!album) {
            return res.status(404).json('Albumia ei löytynyt')
        }
        await Album.findById(albumID)
        res.status(200).json('Albumi poistettiin')
    }
    catch (error) {
        res.status(500).send()
    }
}

// PUT pyyntö jolla päivitetään Albumi tietokannan tietoja
const updateAlbum = async (req, res) => {
    const { albumID } = req.params
    const { artist } = req.query
    const { title } = req.query
    const { year } = req.query
    const { genre } = req.query
    const { tracks } = req.query

    try {
        const album = await Album.findById(albumID)
        if (!album) {
            return res.status(404).json('Albumia ei löytynyt')   
        }

        album.artist = artist
        album.title = title
        album.year = year
        album.genre = genre
        album.tracks = tracks

        const updateAlbum = await album.save()
        return res.status(404).json(updateAlbum)
    }
    catch (error) {
        res.status(500).send()
    }
}

// Post pyyntö luo uuden albumin, albumi tietokantaan

const createAlbum = async (req, res) => {
    const { artist } = req.body
    const { title } = req.body
    const { year } = req.body
    const { genre } = req.body
    const { tracks } = req.body
    const album = new Album({
        artist,
        title,
        year,
        genre,
        tracks
    })
    if (!artist, !title, !year, !genre, !tracks) {
        return res.status(400).json('Albumia ei voitu lisätä koska sille ei annettu kaikkia tietoja')
    }
    try {
        await album.save()
        return res.status(201).json(album)
    }
    catch (error) {
        res.status(400).send()
    }
}

module.exports = { 
    getAlbums,
    getSingleAlbum,
    deleteAlbum,
    createAlbum,
    updateAlbum,
}