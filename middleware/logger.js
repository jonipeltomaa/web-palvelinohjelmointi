// Midleware
const logger = (req, res, next) => {
    console.log(req.query)
    const { search } = req.query
    if (!search) {
      return res.status(401).json('Unauthorized')
    }
    next()
}

module.exports = logger